#!/bin/sh
sed -i \
         -e 's/rgb(0%,0%,0%)/#fdfdfd/g' \
         -e 's/rgb(100%,100%,100%)/#4D4F59/g' \
    -e 's/rgb(50%,0%,0%)/#fdfdfd/g' \
     -e 's/rgb(0%,50%,0%)/#E5ACA8/g' \
 -e 's/rgb(0%,50.196078%,0%)/#E5ACA8/g' \
     -e 's/rgb(50%,0%,50%)/#fdfdfd/g' \
 -e 's/rgb(50.196078%,0%,50.196078%)/#fdfdfd/g' \
     -e 's/rgb(0%,0%,50%)/#4D4F59/g' \
	"$@"
