#!/bin/sh

CPU_CORES=$(nproc)

# Aliases
alias ls="exa"
alias cp="cp -i"
alias rm="rm -i"
alias make='make -j$CPU_CORES'
alias mvi='mpv --config-dir=$HOME/.local/opt/mvi'
