#!/bin/sh


# User specific environment

add_to_path() {
        # lol
        PATH="$1:$PATH"

}
add_to_path "$HOME/.local/bin"
add_to_path "$HOME/bin"
add_to_path "$HOME/.cargo/bin"
add_to_path "/usr/lib/qt6/bin"
add_to_path "$HOME/pkg/flutter/bin"
add_to_path "$HOME/.luarocks/bin"

export PATH
