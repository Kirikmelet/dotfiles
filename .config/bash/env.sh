#!/bin/sh

# Set editor/visual
export VIEWER=bat
export EDITOR=nvim

# For Wayland support

export MOZ_ENABLE_WAYLAND=1

# Java
export JDTLS_HOME=/usr/share/java/jdtls

# . /etc/profile.d/texlive.sh

# WINE Prefixes
# export WINE_HENTAI=/home/troyd/.config/wine/koikatsu

# MPD
export MPD_HOST=localhost
export MPD_PORT=23956

# Chromium
export CHROME_EXECUTABLE=/usr/bin/chromium

# Bitwarden
export BW_SESSION="kWPOT4crjZgByej1zkVXRU8+h81kbmwc1BarCFSKPUBMLuw4RfplhpOCC83LbJLO1Ks1EVADCVBjJVaTCPvI/Q=="

