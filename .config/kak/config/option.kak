# Set theme
colorscheme gruvbox-dark

# Set tab stop
set-option global tabstop 4
set-option global indentwidth 4

# Set scrolloff
set-option global scrolloff 1,3


# Set ripgrep
set-option global grepcmd 'rg --follow --vimgrep'

