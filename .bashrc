# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# source ~/.config/bash/*.sh

for i in $(find $HOME/.config/bash -iname "*.sh"); do
        source "$i"
done

# .bashrc

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '
. "$HOME/.cargo/env"
