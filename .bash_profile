# .bash_profile

# Get the aliases and functions
[ -f $HOME/.bashrc ] && . $HOME/.bashrc

if [[ $TERM == 'linux' ]]; then
        source print-help.sh
fi
. "$HOME/.cargo/env"
